//~ id: 53ff71065075535150371687
//~ token: e9f29ace3e9d359c3eab993353346cfc22bf6419


const int pwmMotorPin = A1; 

const int testPin = D7; 


void setup()
{

pinMode(testPin, OUTPUT);
Spark.function("motor", motorControl);
}
 
void loop()
{
    
}

int motorControl(String args) {
    boolean b; 
    int pwmVal = atoi(args.c_str()); 
    
    //~ //Real code
    if (pwmVal < 60)
        analogWrite(pwmMotorPin,0); 
    else
        analogWrite(pwmMotorPin,pwmVal); 
    return 1; 
    
    //Test Code
    //~ if (pwmVal > 127) 
        //~ b = true; 
    //~ else 
        //~ b = false; 
    //~ digitalWrite(D7, b); 
    //~ return value; 
    
}
