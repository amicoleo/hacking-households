TODO
===========

Gesture controlled fan
-------------------

+ Flash **handWaveMotorControl** to the Sparkcore in the gesture controlled fan. Is uses a library. With Spark-CLI it's flashed by doing
                   
```
spark flash handWaveMotorControl 
```
where **handWaveMotorControl** is the directory, not the **.ino** file. 

+ The fan increases speed when waving a hand and decreases speed when holding a hand still in front of the fan. 

+ The firmware prints some logging over serial for debugging the values from the IR sensor. It's handy for seeing if something is not working fine. 

Internet fan
------------

+ Set Wi-Fi credential to the sparkcore. It didn't work to me using the Mobile App at MAO. I had to connect the sparkcore to the computer via USB and set it manually with 
```
spark serial wifi 
```
+ Flash **internetMotorControl** to the sparkcore 

+ Send me the access token and the deviceID of the sparkcore used for the internet fan. So I can update this [website](http://hackinghouseholds.appspot.com/internetfan) and we can use it for controlling the speed of the fan. 



