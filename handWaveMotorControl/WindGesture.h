#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#elif defined(SPARK)
#include "application.h"
#endif

#ifndef WINDGESTURE
#define WINDGESTURE

#include "math.h"


class WindGesture {
    public: 
        WindGesture(); 
        void setParameters(
                int _minVal, 
                int _maxVal, 
                int _minValDiff = 80, 
                int _minValStable = 1500,
                unsigned long _maxPeaksTimeDiff=500, 
                unsigned long _maxRiseFallTimeDiff = 200, 
                unsigned long _stableUpdateTimeDiff = 500, 
                float _learning= 0.5); 
        void setSensorPin(int _sensorPin); 
        void setEasingParameters(float _easBase, float _easTau, float _easInX, float _easInY); 
        void update(); 
        int getValue(); 
        boolean bPrintLog; 
    private: 
        int inputVal; 
        int outputVal; 
    
        int minVal; 
        int maxVal; 

        //Value filter parameters
        int lastVal ; 
        int filteredval ; 
        float learning ; 

        //Easing parameters
        int raisingCurveVal ; 
        int loweringCurveVal ; 
        boolean bSlowing = false; 
        boolean bSpeeding = false; 

        //Hand waving  - speed up parameters
        unsigned long maxPeaksTimeDiff ; //min delay in ms between consecutive peaks for 
        unsigned long maxRiseFallTimeDiff ; //
        unsigned long lastPeakTime; 

        int minValDiff ; 
        unsigned long tRise ; 
        boolean bRise ; 
        
        //Hand in front - slow down parameters
        int minValStable; 
        unsigned long lastStableUpdateTime;
        unsigned long stableUpdateTimeDiff;
        
        unsigned long lastReadingTime; 
        unsigned long readingDelay; 
        
        int sensorPin; 
        
        //
        float easBase, easTau, easInX, easInY; 
        float easingFunction(float val); 
        
        
        //
        void printLog(); 
        

}; 

#endif
