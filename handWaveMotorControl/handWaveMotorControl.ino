#include "WindGesture.h"

WindGesture wg; 
const int pwmMotorPin = A1; 


void setup()
{
// initialize serial comms
Serial.begin(57600);

pinMode(pwmMotorPin, OUTPUT);

wg.setParameters(55,250);
wg.bPrintLog = true; 
wg.setSensorPin(A2); 
wg.setEasingParameters(2, 40, 210, 40); 
}
 
void loop()
{
    wg.update(); 
    int a = wg.getValue(); 
    Serial.println(a); 
    if (a < 60)
        analogWrite(pwmMotorPin,0); 
    else
        analogWrite(pwmMotorPin,a); 
}
