#include "WindGesture.h"

WindGesture::WindGesture(){
        
        inputVal = 0; 
        outputVal = 0; 
    
        minVal = 80; 
        maxVal = 200; 

        //Value filter parameters
        lastVal = 0; 
        filteredval = 0; 
        learning = 0.05; 

        //Easing parameters
        raisingCurveVal = 1; 
        loweringCurveVal = 1; 
        bSlowing = false; 
        bSpeeding = false; 

        //Hand waving  - speed up parameters
        maxPeaksTimeDiff = 500; //min delay in ms between consecutive peaks for 
        maxRiseFallTimeDiff = 200; //
        lastPeakTime = 0; 

        minValDiff = 80; 
        tRise = 0; 
        bRise = false; 
        
         //Hand in front - slow down parameters
        minValStable = 1500; 
        lastStableUpdateTime = 0;
        stableUpdateTimeDiff = 500;
        
        //
        readingDelay = 30; 
        lastReadingTime = millis(); 
        
        //
        sensorPin = A2; 
        
        //Default easing values
        easBase = 2; 
        easTau = 40; 
        easInX = 210; 
        easInY = 40; 
        
        
        //
        bPrintLog = false; 
    
}


void WindGesture::setParameters(
                int _minVal, 
                int _maxVal, 
                int _minValDiff, 
                int _minValStable ,
                unsigned long _maxPeaksTimeDiff, 
                unsigned long _maxRiseFallTimeDiff , 
                unsigned long _stableUpdateTimeDiff, 
                float _learning) {
    minVal = _minVal; 
    maxVal = _maxVal; 
    
    minValDiff = _minValDiff; 
    minValStable = _minValStable; 
    
    maxPeaksTimeDiff = _maxPeaksTimeDiff; 
    maxRiseFallTimeDiff = _maxRiseFallTimeDiff;
    stableUpdateTimeDiff = _stableUpdateTimeDiff;  
    learning = _learning; 
    
}

void WindGesture::setSensorPin(int _sensorPin) {
    sensorPin = _sensorPin; 
}

void WindGesture::setEasingParameters(float _easBase, float _easTau, float _easInX, float _easInY) {
    easBase = _easBase; 
    easTau = _easTau; 
    easInX = _easInX; 
    easInY = _easInY; 
}


void WindGesture::update() {
    
    if (millis() - lastReadingTime > readingDelay) {
        lastReadingTime = millis(); 
    
        //Hand waving  - speed up
        if (millis() - lastPeakTime > maxPeaksTimeDiff) {
            raisingCurveVal = 1; 
            bSpeeding = false; 
        }

        if (inputVal - filteredval > minValDiff) //Value goes up... 
        {
          tRise = millis(); 
          bRise = true;   
        }
        
        if (bRise) {
            if (inputVal - filteredval < minValDiff)  //... then Value goes down... 
            {
                if  (millis() - tRise < maxRiseFallTimeDiff) //...within a specific time interval...
                {
                    if (millis() - lastPeakTime < maxPeaksTimeDiff) //...and there was another peak before it within maxPeaksTimeDiff interval
                    {
                        bSpeeding = true; 
                        if (outputVal < maxVal) {
                            outputVal+=(raisingCurveVal); 
                            raisingCurveVal++; 
                        }
                    }
                    lastPeakTime = millis(); 
                }
            bRise = false; 
            }
        }
        
        //Hand in front - slow down
        if (!bSpeeding){
            if (filteredval > minValStable) //If filtered value is above minValStable threshold...
            {
                if (millis() - lastStableUpdateTime > stableUpdateTimeDiff){ //...and it was it for more then stableUpdateTimeDiff
                    lastStableUpdateTime = millis();  
                    if (outputVal > minVal ) {
                        outputVal-=easingFunction(loweringCurveVal);
                        loweringCurveVal++; 
                    }
                }
            }
            
            else {
              lastStableUpdateTime = millis();  
              loweringCurveVal = 1; 
            }
        }
        
        if (outputVal < minVal) {
            outputVal = minVal; 
        }
        
        if (outputVal > maxVal) {
            outputVal = maxVal; 
        }
        
        //Read sensor value and filter
        inputVal = analogRead(sensorPin);
        filteredval = inputVal*learning + filteredval*(1-learning); 
        
        //
        if (bPrintLog)
            printLog(); 
    }
}

int WindGesture::getValue() {
    return outputVal; 
}

void WindGesture::printLog() {
        Serial.print("WIND GESTURE: "); 
        Serial.print("Raising curve val.: ");
        Serial.print(raisingCurveVal);
        Serial.print(" - Lowering curve val.: ");
        Serial.print(loweringCurveVal);
        Serial.print(" - Output val.: ");
        Serial.print(outputVal);
        Serial.print(" - Input Val.: ");
        Serial.println(inputVal);
    }


float WindGesture::easingFunction(float val)  {
    //A really specific easing function
    return (pow(easBase, ((val+easInX)/easTau)) - easInY); 
}
